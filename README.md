# Aspect ratio boxes

A predefined set of classes, in order to create boxes with a fixed aspect ratio. Implementation is very straight forward: 

1. Include **ar-boxes.min.css** or **ar-boxes.css** in your header (or paste the CSS into your stylesheet)
2. Use the respective classes in your HTML (see **Examples** for proper implementation).

---
                                                                               
## Examples

An image with a fixed 1:1 aspect ratio:

```html
<div class="ar-box-1-1">
    <img class="ar-fill" src="#" alt="Image with static 1:1 aspect ratio" />
</div>
```

An image with a 1:1 aspect ratio, changing to a 16-9 ratio for tablet and larger screens:

```html
<div class="ar-box-1-1 ar-box-md-16-9">
    <img class="ar-fill" src="#" alt="Responsive image with 1:1 aspect ratio, changing to 16-9 ratio for tablet and larger screens" />
</div>
```

---

## Available classes / ratios

| Class Name | Ratio |
| ------------ | --------- |
| .ar-box-1-1  | 1:1       |
| .ar-box-2-1  | 2:1       |
| .ar-box-1-2  | 1:2       |
| .ar-box-3-1  | 3:1       |
| .ar-box-1-3  | 1:3       |
| .ar-box-3-2  | 3:2       |
| .ar-box-2-3  | 2:3       |
| .ar-box-4-3  | 4:3       |
| .ar-box-3-4  | 3:4       |
| .ar-box-8-5  | 8:5       |
| .ar-box-5-8  | 5:8       |
| .ar-box-16-9 | 16:9      |
| .ar-box-9-16 | 9:16      |
| .ar-fill     | fill box  |

**HINT:** To have the child element fill the entire box (for example an image which should maintain a given aspect ratio), add the class **.ar-fill** to said child element.

---


## Responsive classes

Based on the Bootstrap breakpoints and naming conventions, the aspect ratios can be defined relative to the screen width.

| Naming Convention | Breakpoint (min-width) |
| ----------------- | ---------------------- |
|  .ar-box-sm-X-X   |         576px          |
|  .ar-box-md-X-X   |         768px          |
|  .ar-box-lg-X-X   |         992px          |
|  .ar-box-xl-X-X   |        1200px          |   

**HINT:** This can also be applied to **.ar-fill**.